<?php
namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        User::insert([
            'name' => 'user name',
            'user_name' => 'coba',
            'password' => Hash::make('user123'),
            'updated_by' => 'seeder',
            'created_by' => 'seeder',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
